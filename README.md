# Relatório

1. [Tecnologias Utilizadas](#tecnologias-utilizadas)
2. [Diagrama da Solução](#diagrama-da-solução)
3. [Frameworks Utilizados](#frameworks-utilizados)
4. [Endereços](#endereços)

## Tecnologias Utilizadas

O desenvolvimento do case foi feito utilizando a linguagem JavaScript. Como IDE foi utilizado o VS Code.

Foi utilizado a plataforma Docker para realizar o empacotamento das aplicações desenvolvidas. As imagens dos containers gerados foram baseadas no sistema operacional Alpine, com o objetivo de otimização do tamanho da imagem do container.

Para realizar a persistência dos dados foi utilizado o banco de dados NoSQL Apache 
CouchDB (https://couchdb.apache.org/).

O pipeline foi construído utilizando o [Gitlab-CI](https://about.gitlab.com/).

Os testes foram escritos utilizando o framework Jest e Supertest. Maiores detalhes no item "Frameworks Utilizados".

Por fim, para fazer o Deployment da aplicação foi utilizado Kubernetes.

## Diagrama da Solução

Para o desenvolvimento do Case foram construídas duas aplicações. A primeira aplicação trata-se do front-end, desenvolvida utilizando o framework VueJS. A segunda  aplicação trata-se do back-end, desenvolvido utilizando a plataforma NodeJS.

Algumas justificativas para o motivo das duas aplicações são: primeiro é conseguir utilizar as melhores tecnologias em cada camada e a segunda é mitigar os riscos e impacto no usuário final nos momentos de manutenção da  aplicação.

![diagram](img/diagram.png)

## Frameworks Utilizados

Para a aplicação do back-end foram utilizados os seguintes frameworks:

- Express versão 4.17.1
- Jest versão 24.9.0
- Supertest versão 4.0.2

Foram realizados testes das rotas do back-end utilizando os frameworks `Jest` e `Supertest`.

Já para a aplicação do front-end foram utilizados os seguintes frameworks:

- VueJS versão 2.5.11
- Webpack versão 3.6.0

## Endereços

Perfil DockerHub

Usuário: anardy

Endereço: [https://hub.docker.com/u/anardy](https://hub.docker.com/u/anardy)

As imagens docker geradas e publicadas no DockerHub são:

- [anardy/backend_casetwitter](https://hub.docker.com/r/anardy/backend_casetwitter)
- [anardy/frontend_casetwitter](https://hub.docker.com/r/anardy/frontend_casetwitter)